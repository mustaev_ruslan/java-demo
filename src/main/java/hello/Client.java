package hello;

import javax.persistence.*;

/**
 * Created by Руслан on 03.03.2017.
 */
@Entity
@SequenceGenerator(name = "sequence", sequenceName = "client_ids")
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence")
    private Long id;

    private String name;

    private String city;

    private String phone;

    private Integer age;

    public Boolean isAdult() { return age >= 18; }

    public Integer adultYears() { return age - 18;}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        try {
            if (age <= 0 || age >= 120) {
                throw new AgeException();
            }
            this.age = age;
        }
        catch (AgeException ex){
            System.out.println(ex.errorMessage);
        }
    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}

class AgeException extends Exception {
    String errorMessage;
    public AgeException(){
        this.errorMessage = "Age is not valid!";
    }
}