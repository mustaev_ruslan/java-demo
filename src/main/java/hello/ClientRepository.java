package hello;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by Руслан on 03.03.2017.
 */
@RepositoryRestResource(collectionResourceRel = "client", path = "clients")
public interface ClientRepository extends PagingAndSortingRepository<Client, Long> {
}
