package hello;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DatabaseExampleApplicationTests {

	@Autowired
	private ClientsController controller;

	@Test
	public void contexLoads() throws Exception {
		assertThat(controller).isNotNull();
	}


	@Test
	public void isAdult15Test() throws Exception {
		Client client = new Client();
		client.setAge(15);
		Assert.isTrue(client.isAdult()==false, "isAdult15 failed");
	}

	@Test
	public void isAdult30Test() throws Exception {
		Client client = new Client();
		client.setAge(30);
		Assert.isTrue(client.isAdult()==true, "isAdult30 failed");
	}

}
