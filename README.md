# README #

### Used technologies ###
* Spring MVC
* Spring REST
* PostgreSQL
* Thymeleafe
* Bootstrap
* jQuery
* Ajax
* Tomcat
* InteliJ IDEA

### REST API for curl###

* Show all clients: curl http://localhost:8080/clients
* Add new client: curl -i -X POST -H "Content-Type:application/json" -d "{ \"name\" : \"Ivanov Ivan\",  \"age\" : \"30\", \city\" : \"Moscow\", \"phone\" : \"+7(123)456-78-90\"   }" http://localhost:8080/clients
* Edit client: curl -X PUT -H "Content-Type:application/json" -d "{ \"name\": \"Petrov Petr\" }" http://localhost:8080/clients/{id}
* Delete client: curl -X DELETE http://localhost:8080/clients/{id}

### Screenshots for http://localhost:8080/clientsview ###
![1.png](https://bitbucket.org/repo/LyG4yX/images/4227168012-1.png)
![Add.png](https://bitbucket.org/repo/LyG4yX/images/2998995493-Add.png)
![Edit.png](https://bitbucket.org/repo/LyG4yX/images/1920873591-Edit.png)
![Delete.png](https://bitbucket.org/repo/LyG4yX/images/2697566666-Delete.png)